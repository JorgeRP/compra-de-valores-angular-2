import { NgModule }      from '@angular/core';
import { MaterializeModule } from 'angular2-materialize'
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { BuscadorComponent } from './compra/components/formulario/components/buscador/buscador.component';
import { CompraComponent }  from './compra/compra.component';
import { FormularioComponent } from './compra/components/formulario/formulario.component';
import { ConfirmacionComponent } from './compra/components/confirmacion/confirmacion.component';
import { HomeComponent }  from './home/home.component';
import { NavigatorComponent } from './navigator/navigator.component';
import { DatosComponent } from './home/datos/datos.component';
import { TableComponent } from './table/table.component';
import { Main } from './index'
import { CompraModel } from './compra/share/compra.model';
import { RouterModule, Routes } from '@angular/router';
import { DatosService } from './home/datos/share/datos.service';
import { CompraService } from './compra/share/compra.service';
import { FormularioService } from './compra/components/formulario/share/formulario.service';
import { ReferenciaService } from './compra/components/share/referencia.service';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';
import { HttpModule }    from '@angular/http';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

const appRoutes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'compra',
    component: CompraComponent,
    children: [
      {
        path: 'formulario',
        component: FormularioComponent
      },
      {
        path: 'confirmacion',
        component: ConfirmacionComponent
      }
    ]
  },
  {
    path: '',
    component: Main
  }
];

@NgModule({
  declarations: [
    CompraComponent,
    HomeComponent,
    Main,
    NavigatorComponent,
    DatosComponent,
    TableComponent,
    FormularioComponent,
    ConfirmacionComponent,
    BuscadorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MaterializeModule,
    HttpModule,
    InMemoryWebApiModule.forRoot(InMemoryDataService),
    RouterModule.forRoot(appRoutes),
    Ng2AutoCompleteModule,
    BrowserAnimationsModule
  ],
  providers: [
    DatosService,
    CompraService,
    CompraModel,
    FormularioService,
    ReferenciaService
  ],
  bootstrap: [Main]
})
export class AppModule { }
