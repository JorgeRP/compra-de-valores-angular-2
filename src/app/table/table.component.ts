import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { DatosService } from '../home/datos/share/datos.service'

@Component({
  selector: 'table-one',
  templateUrl: './table.component.html'
})
export class TableComponent implements OnChanges {
  @Input() data;
  @Input() from: String;
  dataToShow: Object;

  ngOnChanges(changes: SimpleChanges) {
    this.dataToShow = changes["data"].currentValue;
    if(changes["from"]) {
        this.from = changes["from"].currentValue;
    }
  }
  checkIfIsArray(element) {
    return Array.isArray(element);
  }

  checkIfThereIsObject(element) {
    return typeof element[0] === "object";
  }
}
