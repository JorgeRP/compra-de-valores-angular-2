import { ImporteModel } from './importe.model';

export class CompraModel {
  numeroTitulos: number;
  numeroTitulosOcultos: number;
  fecha: string;
  tipoOrden: Object;
  precioTitulo: ImporteModel;
  precioActivacion: ImporteModel;

  constructor() {}
}
