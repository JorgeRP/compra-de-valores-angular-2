import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'oip-valores-compra',
  templateUrl: './compra.component.html',
  styleUrls: ['./compra.component.css']
})
export class CompraComponent {

  data: Object;
  variableModule: Object;

  constructor(public router: Router) {
  }

  ngOnInit(){
      this.router.navigate(['compra/formulario'])
  }
}
