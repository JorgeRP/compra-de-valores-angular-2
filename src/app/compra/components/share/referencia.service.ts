import { Injectable } from '@angular/core';

@Injectable()
export class ReferenciaService {

  infoReferencia: Object;

  constructor() {}

  setInfoReferencia(data) {
    this.infoReferencia = data;
  }

  getInfoReferencia() {
    return this.infoReferencia;
  }

  cleanInfoReferencia() {
    this.infoReferencia = {};
  }
}
