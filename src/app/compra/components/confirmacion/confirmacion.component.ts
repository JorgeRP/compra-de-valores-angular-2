import { Component } from '@angular/core';
import { ReferenciaService } from '../share/referencia.service';

@Component({
  selector: 'oip-valores-compra-confirmacion',
  templateUrl: './confirmacion.component.html',
  styleUrls: ['./confirmacion.component.css']
})
export class ConfirmacionComponent {

  referenciaData: Object;

  constructor(private referenciaService: ReferenciaService) {};

  cleanInfoReferencia() {
    this.referenciaService.cleanInfoReferencia();
  }

  getInfoReferencia() {
    this.referenciaData = this.referenciaService.getInfoReferencia();
  }

  ngOnInit() {
    this.getInfoReferencia();
    this.cleanInfoReferencia();
  }

}
