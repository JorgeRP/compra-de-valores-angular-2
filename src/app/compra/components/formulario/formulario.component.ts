import { Component, Output, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators  } from '@angular/forms';
import {style, state, animate, transition, trigger} from '@angular/core';
import { FormularioService } from './share/formulario.service';
import { ReferenciaService } from '../share/referencia.service';
import { CompraModel } from '../../share/compra.model';


@Component({
  selector: 'oip-valores-compra-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css'],
  animations: [
  trigger('fadeInOut', [
    transition(':enter', [
      style({opacity:0}),
      animate(200, style({opacity:1}))
    ]),
    transition(':leave', [
      animate(200, style({opacity:0}))
    ])
  ])
]
})
export class FormularioComponent implements OnChanges {
  valor: Object;
  datosValor: Object;
  compraForm: FormGroup;
  focusNumeroTitulos: boolean;
  focusFecha: boolean;
  focusPrecioTitulo: boolean;
  focusPrecioActivacion: boolean;
  numeroTitulosMinError: boolean;
  tiposOrdenesMercado: Array<any>;
  codigoTipoOrdenLimitada: string;
  divisor: string;
  maxDecimals: any;
  maxDecimalsPattern: string;
  precioActivacionActivado: boolean = false;
  importVariable: string;
  numberOfDecimals: number;
  importNumber: number;
  solicitudCompra: CompraModel;
  referencia: {
      referenciaOrden: string;
      tipoReferencia: string;
  }

  // ------------------------------------CONSTRUCTOR-------------------------------------------------

  constructor(public router: Router, fb: FormBuilder, private formularioService: FormularioService, private referenciaService: ReferenciaService) {
    this.compraForm = fb.group({
      "numeroTitulos": [null, Validators.compose([Validators.required, Validators.min(1), Validators.max(99999)])],
      "numeroTitulosOcultos": '',
      "fecha": [null, Validators.required],
      "tipoOrden": [null, Validators.required],
      "precioTitulo":null,
      'inidicadorPrecioActivacion':false,
      "precioActivacion": null,
    });

    this.codigoTipoOrdenLimitada = '3';
    this.divisor = "1";
  }

  // ------------------------------------------------REQUESTS----------------------------------------

  getTiposOrdenesMercado() {
    this.formularioService.getTiposOrdenesMercado().subscribe( response => {
      this.tiposOrdenesMercado = response.data;
    })
  }

  // --------------------------------------------------FUNCTIONS----------------------------------------

  goToSecondStep() {
    this.router.navigate(['../compra/confirmacion'])
  }

  ngOnChanges(changes: SimpleChanges) {
    this.datosValor = changes["valor"].currentValue;
    this.getDecimals(this.datosValor['lastPrice'].amount);
  }

  subscribeToChanges(inputToObserve, inputToChange) {
    this.compraForm.controls[inputToObserve].valueChanges.subscribe((element) => {
      switch (inputToChange) {
        case 'precioTitulo':
          this.changeInputValidator(inputToChange, element.codigotporden === this.codigoTipoOrdenLimitada);
          break;
        case 'precioActivacion':
          this.changeInputValidator(inputToChange, element);
          break;
      }
    })
  }

  changeInputValidator(inputToChange, condition) {
    if(condition){
      this.compraForm.controls[inputToChange].setValidators([Validators.required]);
    } else {
      this.compraForm.controls[inputToChange].clearValidators();
      this.clearValue(inputToChange);
    }
    this.compraForm.controls[inputToChange].updateValueAndValidity();
  }

  clearValue(inputName) {
    this.compraForm.controls[inputName].setValue(null);
  }

  getDecimals(number) {
    if(number.indexOf(',') > -1) {
      this.formMaxDecimals(number.split(',')[1].length);
      this.formMaxDecimalsPattern(number.split(',')[1].length);
    }
    if(number.indexOf('.') > -1) {
      this.formMaxDecimals(number.split('.')[1].length);
      this.formMaxDecimalsPattern(number.split('.')[1].length);
    }
  }

  formMaxDecimals(decimalsLength) {
    for(let i = 0; i <= decimalsLength; i++) {
      this.divisor = this.divisor.concat('0');
    }
    this.maxDecimals = 1/Number(this.divisor);
  }

  formMaxDecimalsPattern(decimalsLength) {
    this.maxDecimalsPattern = '[0-9]+(\\.[0-9]{1,' + decimalsLength + '})?';
  }

  formatImport(number) {
    this.importVariable = number.toString();
    this.numberOfDecimals = this.getNumberOfDecimals(this.importVariable);
    this.importNumber = this.numberWithoutDotOrComma(this.importVariable);

    return {
      importeConSigno: this.importNumber,
      numeroDecimalesImporte: this.numberOfDecimals
    }
  }

  getNumberOfDecimals(stringNumber) {
    if(stringNumber.indexOf(',') > -1) {
      return Number(stringNumber.split(',')[1].length);
    }
    if(stringNumber.indexOf('.') > -1) {
      return Number(stringNumber.split('.')[1].length);
    }
  }

  numberWithoutDotOrComma(stringNumber) {
    if(stringNumber.indexOf(',') > -1) {
        return Number(stringNumber.split(',')[0] + stringNumber.split(',')[1]);
    }
    if(stringNumber.indexOf('.') > -1) {
        return Number(stringNumber.split('.')[0] + stringNumber.split('.')[1]);
    }
  }

  captureEvent(value) {
    this.valor = [value];
    this.getDecimals(this.valor[0]['lastPrice'].amount);
  }

  onBlur(inputName) {
    if(inputName === 'numeroTitulos'){
      this.focusNumeroTitulos = false;
    }
    if(inputName === 'fecha') {
      this.focusFecha = false;
    }
    if(inputName === 'precioTitulo') {
      this.focusPrecioTitulo = false;
    }
    if(inputName === 'precioActivacion') {
      this.focusPrecioActivacion = false;
    }
  }

  onFocus(inputName) {
    if(inputName === 'numeroTitulos') {
      this.focusNumeroTitulos = true;
    }
    if(inputName === 'fecha') {
      this.focusFecha = true;
    }
    if(inputName === 'precioTitulo') {
      this.focusPrecioTitulo = true;
    }
    if(inputName === 'precioActivacion') {
      this.focusPrecioActivacion = true;
    }
  }

  createCompraObject() {
    return this.solicitudCompra = {
      numeroTitulos: this.compraForm.controls['numeroTitulos'].value,
      numeroTitulosOcultos: this.compraForm.controls['numeroTitulosOcultos'].value,
      fecha: this.compraForm.controls['fecha'].value,
      tipoOrden: this.compraForm.controls['tipoOrden'].value,
      precioTitulo: this.compraForm.controls['tipoOrden'].value.codigotporden === this.codigoTipoOrdenLimitada ? this.formatImport(this.compraForm.controls['precioTitulo'].value) : null,
      precioActivacion: this.compraForm.controls['inidicadorPrecioActivacion'].value ? this.formatImport(this.compraForm.controls['precioActivacion'].value) : null
    }
  }

  createReferenciaObject(response) {
    this.referencia = {
      "referenciaOrden": response.data.referenciaOrden,
      "tipoReferencia": response.data.tipoReferencia
    }

    return this.referencia;
  }

  sendReferenciaData(data) {
    this.referenciaService.setInfoReferencia(data);
  }

  goToStepTwo() {
    this.router.navigate(['compra/confirmacion']);
  }

  onSubmit(value: any):void {
    if(value) {
      this.formularioService.solicitudompraValores(this.createCompraObject()).subscribe( response => {
        if(!response.data.hasOwnProperty("referenciaOrden") || !response.data.hasOwnProperty("tipoReferencia")) {
          response.data = {
            "referenciaOrden": "123456",
            "tipoReferencia": "V2"
          }
        }
        this.sendReferenciaData(this.createReferenciaObject(response));
        this.goToStepTwo();
      })
    }
  }

  // ----------------------------------------INIT------------------------------------------

  ngOnInit() {
    this.getTiposOrdenesMercado();
    this.subscribeToChanges('tipoOrden','precioTitulo');
    this.subscribeToChanges('inidicadorPrecioActivacion','precioActivacion')
  }
}
