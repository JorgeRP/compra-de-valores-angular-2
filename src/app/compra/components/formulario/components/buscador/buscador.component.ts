import { Component, EventEmitter, Output, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { CompraService } from '../../../../share/compra.service';
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";

@Component({
  selector: 'oip-valores-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.css']
})
export class BuscadorComponent {
  @Output() valor = new EventEmitter();
  data: Object;
  variableModule: Object;

  constructor(public router: Router, public compraService: CompraService, private _sanitizer: DomSanitizer) {
    this.compraService.getter().subscribe((response)=>{
      this.data = response.data;
    })
  }

  sendValorToParent(newValue) {
    this.valor.emit(newValue);
  }


  autocompleListFormatter = (data: any) : SafeHtml => {
    let html = `<span>${data.name}</span>`;
    return this._sanitizer.bypassSecurityTrustHtml(html);
  }
}
