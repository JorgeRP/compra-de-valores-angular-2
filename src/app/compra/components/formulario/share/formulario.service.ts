import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';

@Injectable()
export class FormularioService {

  private datosUrl = 'app/tiposOrdenesMercado';
  private SOLICITUD_COMPRA_URL = 'app/solicitudompraValores';

  constructor(private http: Http) {}
  getTiposOrdenesMercado (): Observable<any> {

    return this.http.get(this.datosUrl).map(
      response => response.json()
    )

  }

  solicitudompraValores(model):  Observable<any> {
    return this.http.post(this.SOLICITUD_COMPRA_URL, model).map(
      response => response.json()
    )
  }

  private handleError(error: any) {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
