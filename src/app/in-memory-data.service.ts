import { InMemoryDbService } from 'angular-in-memory-web-api';
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const infoBolsaArray = [
      {
            name: 'BBVA',
            lastPrice: {
              amount: '12,64',
              badge: '€'
            },
            difference: '2',
            differencePorcentage: '26',
            max: '15,46',
            min: '9,76'
      },
      {
            name: 'TELEFÓNICA',
            lastPrice: {
              amount: '10,21',
              badge: '€'
            },
            difference: '5',
            differencePorcentage: '30',
            max: '12,64',
            min: '10,33'
      },
      {
            name: 'ABANCA',
            lastPrice: {
              amount: '122,64',
              badge: '€'
            },
            difference: '23',
            differencePorcentage: '6',
            max: '20,46',
            min: '1,76'
      }
    ]

    const empresasArray = [
      {
            name: 'BBVA',
            lastPrice: {
              amount: '12,64',
              badge: '€'
            },
            difference: '2',
            differencePorcentage: '26',
            max: '15,46',
            min: '9,76'
      },
      {
            name: 'TELEFÓNICA',
            lastPrice: {
              amount: '10,21',
              badge: '€'
            },
            difference: '5',
            differencePorcentage: '30',
            max: '12,64',
            min: '10,33'
      },
      {
            name: 'ABANCA',
            lastPrice: {
              amount: '122,64',
              badge: '€'
            },
            difference: '23',
            differencePorcentage: '6',
            max: '20,46',
            min: '1,76'
      },
      {
            name: 'BANKIA',
            lastPrice: {
              amount: '12,644',
              badge: '€'
            },
            difference: '2',
            differencePorcentage: '26',
            max: '15,464',
            min: '9,764'
      },
      {
            name: 'JAZZTEL',
            lastPrice: {
              amount: '10,21',
              badge: '€'
            },
            difference: '5',
            differencePorcentage: '30',
            max: '12,64',
            min: '10,33'
      },
      {
            name: 'BANKINTER',
            lastPrice: {
              amount: '122,64',
              badge: '€'
            },
            difference: '23',
            differencePorcentage: '6',
            max: '20,46',
            min: '1,76'
      }
    ]

    const tiposOrdenesMercado = [
      {
        "name": 'Por lo mejor',
        "codigotporden": '1'
      },
      {
        "name": 'Mercado',
        "codigotporden": '2'
      },
      {
        "name": 'Limitada',
        "codigotporden": '3'
      }
    ]

    const solicitudompraValores = [
      {
        "referenciaOrden": "123456",
        "tipoReferencia": "V2"
      }
    ]
    return {
      infoBolsaArray,
      empresasArray,
      tiposOrdenesMercado,
      solicitudompraValores
    };
  }
}
