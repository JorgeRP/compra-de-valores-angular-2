import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './index.html',
  styleUrls: ['./app.component.css']
})
export class Main {

  constructor(public router: Router) {}

  ngOnInit(){
      this.router.navigate(['/home'])
  }

}
