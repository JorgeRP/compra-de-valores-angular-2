import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';

@Injectable()
export class DatosService {

  private datosUrl = 'app/infoBolsaArray';

  constructor(private http: Http) {}
  getter (): Observable<any> {

    return this.http.get(this.datosUrl).map(
      response => response.json()
    )

  }

  private handleError(error: any) {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
