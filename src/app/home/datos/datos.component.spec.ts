import { TestBed, async } from '@angular/core/testing';
import { DatosComponent } from './datos.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DatosService } from './share/datos.service';

describe('DatosComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [
        DatosComponent
      ],
      providers: [
        DatosService
      ]
    }).compileComponents();
  }));

  it('should create the DatosComponent', async(() => {
    const fixture = TestBed.createComponent(DatosComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
