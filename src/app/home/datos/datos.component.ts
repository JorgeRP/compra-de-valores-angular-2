import { Component } from '@angular/core';
import { DatosService } from './share/datos.service';

@Component({
  selector: 'oip-valores-home-datos',
  templateUrl: './datos.component.html',
  styleUrls: ['./datos.component.css']
})
export class DatosComponent {

  infoBolsa = [];

  constructor(private infoBolsaService: DatosService) {}

  private obtainData() {
    this.infoBolsaService.getter().subscribe(
      response => {
          this.infoBolsa = response.data;
          // var interval = setInterval(() => {
          //   this.sendMoreData();
          //   if(this.infoBolsa.length >15) {
          //     clearInterval(interval);
          //   }
          // }, 3000)
      }
    )
  }

  private sendMoreData() {
    this.infoBolsaService.getter().subscribe(
      response => {
        this.infoBolsa = this.infoBolsa.concat(response.data);
      }
    )
  }

  ngOnInit() {
    this.obtainData();
  }
}
